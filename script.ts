//fighter interface
interface Fighter {
    name: string,
    hp: number,
    acc: number,
    def: number,
    attDc: number,
    str: number,
    accDc: number,
    init: number
}

abstract class Person implements Fighter {

    constructor(public name: string, hp: number, public acc: number, public def: number,
        public attDc: number, public str: number, public accDc: number, public init: number) {
            this._hpCurrent = this.maxHp = hp;
    }

    private _hpCurrent: number;
    public maxHp: number;
    //this provide value for checkup in loop at combat() - Dragon and Warrior inherits from Person
    public isAlive = true;

    public get hp(): number {
        return this._hpCurrent;
    }

    public set hp(_hp: number){
        this._hpCurrent = _hp;
        this.isAlive = this._hpCurrent > 0;
    }

    abstract hit(): void;
    abstract atk(target: Person): void;
    abstract specialAtk(target: Array<Person>);
}

class Warrior extends Person {
    
    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }

    hit(): boolean {
        return this.acc > rollDice(100);
    };

    atk(target: Person): void {
        if (this.hit()) {
            let attackValue = (this.str + rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        } else {
            console.log(`${this.name} miss`);
        }
    };

    specialAtk(target: Person[]) {
        throw new Error("Method not implemented.");
    }
}

class Healer extends Warrior {
    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }
    //custom function we provide array of persons
    heal(target: Person[]): any {
        for(let i=0; i<target.length; i++){
            target[i].hp += 30;
        }
    }
}

class Dragon extends Person {

    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }

    hit(): boolean {
        return this.acc > rollDice(100);
    };

    atk(target: Person): void {
        if (this.hit()) {
            let attackValue = (this.str + rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        } else {
            console.log(`${this.name} miss`);
        }
    };

    specialAtk(target: Person[]) {
        for(let i=0; i<target.length; i++){
            this.atk(target[i]);
        }
    }
}

//takes value from initCalc and multiply for random Math value
function rollDice(max: number): number {
    return Math.floor((Math.random() * max) + 1);
}

//provide value to rolldice function
function initCalc(): number {
    return rollDice(10);
}

function combat(): any {
    //creation of array for team
    let team: Array<Warrior>;
    //filling out team array
    team = [
        //(name, hp, acc, def, attDc, str, accDc, init)
        new Warrior("War1", 100, 80, 10, 10, 100, 10, 10),
        new Warrior("War2", 100, 90, 10, 10, 100, 10, 10),
        new Warrior("War3", 100, 80, 10, 10, 100, 10, 10),
        new Warrior("War4", 100, 70, 10, 10, 100, 10, 10),
        new Healer("Heal1", 100, 70, 10, 10, 100, 10, 10)
    ];

    let dragon = new Dragon("Dragon", 1000, 90, 10, 100, 30, 10, 10);

    while (true) {
        //counter to keep values from loop - checkup is death team members is equal for team.length 
        let counter = 0;
        //checks if any team member or dragon is alive - attack
        for (let i = 0; i < team.length; i++) {
            if(team[i].isAlive && dragon.isAlive){
                //if team member is healer - heals
                if(team[i] instanceof Healer){
                    //projection of team[i] to Healer class
                    (team[i] as Healer).heal(team);
                    console.log(team[i].name + " heals")
                }
                //team attack dragon
                team[i].atk(dragon);
                console.log("dragon hp is: " + dragon.hp);
            }
            //counter to check is all team members are dead
            else{
                counter++;
                console.log("Team member named " + team[i].name + " is dead");
            } 
        }
        //if dead break the loop
        if(counter == team.length) break;
        //if dargon is alive perform special attack
        if(dragon.isAlive){
            dragon.specialAtk(team);
        //if dragon is dead break
        }else{
            break;
        }
    }
}
//start the loop in combat function
combat();