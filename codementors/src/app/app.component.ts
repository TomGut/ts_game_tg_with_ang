import { Component } from '@angular/core';
import { Todo } from './todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //all info from todo and todo list gets here for orchestriation
  todos: Array<Todo>;
  currentTodo: Todo;

  constructor(){
    this.todos = [];
    this.currentTodo = new Todo('', '');
  }
  //single todo is added to array - to todo list component
  add(todo: Todo){
    this.todos.push(todo);
  }

  select(todo: Todo){
    this.currentTodo = todo;
  }
}
