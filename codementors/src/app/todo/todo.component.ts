import { Component, OnInit, Input, Output } from '@angular/core';
//importing class from separate file
import { Todo } from '../todo';
//must be from "@angular/core"
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  /*
  komunikacja w app po adnotacjach |@Input i @Output

  todo <-- APP COMPOPNENT --> todo list

  */

  //when we add new todo in website input - everything goes to app.component and list is updated
  @Input()
  todo: Todo;

  //when todo added event is emitted to higher layer
  @Output()
  todoAdded: EventEmitter<Todo>;

  //EventEmitter for passing by event to higher layer
  constructor() { 
    this.todoAdded = new EventEmitter();
  }

  ngOnInit() {
  }

  add(todo: Todo){
    //when addevent then its emitted to higher layer + to get separate list el we create new Todo and 
    //keep values from input in constructor
    this.todoAdded.emit(new Todo(todo.title, todo.note));
    //we clear input fields
    this.todo.title = '';
    this.todo.note = '';
  }
}
