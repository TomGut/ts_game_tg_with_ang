import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  //array from parent - app
  @Input()
  todos: Array<Todo>;

  //after receival produces event and sending it to higher layer
  @Output()
  todoSelected: EventEmitter<Todo>;

  constructor() { 
    this.todoSelected = new EventEmitter();
  }

  ngOnInit() {
  }

  //emit event to higher layer
  selectTodo(todo: Todo){
    this.todoSelected.emit(todo);
  }

}
